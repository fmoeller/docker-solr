[Origin](https://github.com/docker-solr/docker-solr/blob/master/README.md)

# How to use this Docker image

To run a single Solr server:

```console
$ docker run --name my_solr -d -p 8983:8983 -t xrowgmbh/solr:latest
```

Then with a web browser go to `http://localhost:8983/` to see the Admin Console (adjust the hostname for your docker host).

In the web UI if you click on "Core Admin" you should now see the "gettingstarted" core.