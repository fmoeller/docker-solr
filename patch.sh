#!/bin/bash

SOLR_CONFIG=/opt/solr/server/solr/configsets/ezplatform

sed -i '
/<\/config>/ i\
<searchComponent name="elevator" class="solr.QueryElevationComponent" > \
  <!-- pick a fieldType to analyze queries --> \
  <str name="queryFieldType">string<\/str> \
  <str name="config-file">elevate.xml<\/str> \
</searchComponent> \
' ${SOLR_CONFIG}/solrconfig.xml

sed -i '
/<requestHandler name="\/select" class="solr.SearchHandler">/ a\
  <arr name="last-components"> \
    <str>elevator</str> \
  <\/arr> \
' ${SOLR_CONFIG}/solrconfig.xml