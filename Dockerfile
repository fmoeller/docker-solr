FROM solr:8.5.0

MAINTAINER "Björn Dieding" <bjoern@xrow.de>

ENV SOLR_VERSION=8.5.0
ENV VERSION=3.0.0
ENV SOLR_HEAP=800m

USER root

RUN curl -L -o source.zip https://github.com/ezsystems/ezplatform-solr-search-engine/archive/v$VERSION.zip && \
    unzip source.zip && \
    sed -ie 's/root/bla/' ezplatform-solr-search-engine-$VERSION/bin/generate-solr-config.sh && \
    ./ezplatform-solr-search-engine-$VERSION/bin/generate-solr-config.sh --solr-install-dir=$(pwd) --destination-dir=$(pwd)/server/solr/configsets/ezplatform

COPY elevate.xml /opt/solr/server/solr/configsets/ezplatform/conf/elevate.xml
COPY patch.sh patch.sh
RUN sh patch.sh

USER solr

CMD ["bash", "-c", "precreate-core ezplatform server/solr/configsets/ezplatform && solr-foreground -force"]

